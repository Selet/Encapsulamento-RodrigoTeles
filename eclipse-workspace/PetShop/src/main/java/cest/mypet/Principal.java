package cest.mypet;

import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.Pessoa;
import cest.mypet.cadastro.TipoAnimal;
import cest.mypet.cadastro.loc.Cidade;
import cest.mypet.cadastro.loc.UF;

public class Principal {

	public static void main(String[] args) {
		Animal anim = new Animal();
		anim.setNome("Lulu");
		anim.setIdade(2);
		
		TipoAnimal raca = new TipoAnimal();
		raca.setDescricao("Puddle");
		raca.setCod(111);;
		
		Pessoa person = new Pessoa();
		person.setNome("Glaydson");
		person.setIdade(32);
		
		Cidade city = new Cidade();
		city.setNome("Acre");
		
		UF pass = new UF();
		pass.setCod("11");
		pass.setDescricao("AC");
		System.out.println("######### ANIMAL #########");
		System.out.println("Nome do cachorro: " + anim.getNome());
		System.out.println("Idade do cachorro: " + anim.getIdade());
		System.out.println("Descrição do animal: " + raca.getDescricao());
		System.out.println("Código do animal: " + raca.getCod());
		
		System.out.println("######### PESSOA #########");
		System.out.println("Nome da pessoa: " + person.getNome());
		System.out.println("Idade da pessoa: " + person.getIdade());
		System.out.println("Nome da cidade: " + city.getNome());
		System.out.println("Código da UF: " + pass.getCod());
		System.out.println("Descrição da UF: " + pass.getDescricao());
	}

}

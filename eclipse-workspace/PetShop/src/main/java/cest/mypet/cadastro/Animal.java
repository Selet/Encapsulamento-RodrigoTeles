package cest.mypet.cadastro;

public class Animal {
	private String nome;
	private int idade;
	private TipoAnimal tipo;
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public int getIdade() {
		return idade;
	}
	
	public void setTipoAnimal(TipoAnimal tipo) {
		this.tipo = tipo;
	}
	public TipoAnimal getTipoAnimal() {
		return tipo;
	}
}
